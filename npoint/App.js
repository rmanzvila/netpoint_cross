/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React, {Component} from 'react';
import {Platform, SafeAreaView, StyleSheet, Text, View, Image, ImageBackground, TouchableOpacity} from 'react-native';
import {Card, ListItem, Button, Icon} from 'react-native-elements'
import LottieView from "lottie-react-native";
import * as Animatable from "react-native-animatable";


const instructions = Platform.select({
    ios: 'Press Cmd+R to reload,\n' + 'Cmd+D or shake for dev menu',
    android:
        'Double tap R on your keyboard to reload,\n' +
        'Shake or press menu button for dev menu',
});

type
Props = {};
export default class App extends Component<Props> {

    static navigationOptions = ({navigation}) => ({
        headerTransparent: true,
    });

    componentDidMount() {

        //TODO Checar si esta logeado
        this.animation.play();
        setTimeout(() => {
            this.props.navigation.navigate('App');
        }, 4000);

    }

    onLogin = () => {
        this.props.navigation.navigate('App')
    }


    render() {
        const users = [
            {
                name: 'brynn',
                avatar: 'https://s3.amazonaws.com/uifaces/faces/twitter/brynn/128.jpg'
            },
        ]


        return (
            <ImageBackground
                source={{uri: 'https://images.unsplash.com/photo-1534030665069-90e016e995e5?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&w=1000&q=80'}}
                style={styles.container}>
                <SafeAreaView style={{flex: 1}}>
                    <View style={styles.boxText}>
                        <Animatable.Text
                            animation="bounceIn"
                            iterationCount={0}
                            direction="normal"
                            style={styles.welcome}>Bienvenido
                        </Animatable.Text>
                        <Animatable.Text
                            animation="bounceIn"
                            iterationCount={0}
                            direction="normal"
                            style={styles.description}>Traslados garantizados sin importar la distancia y climo
                        </Animatable.Text>
                    </View>
                    <View style={styles.box}>
                        <View style={styles.main}>
                            <View style={styles.boxContent}>
                                <LottieView
                                    ref={animation => {
                                        this.animation = animation;
                                    }}
                                    source={require('./1708-success.json')}
                                />
                            </View>
                            <TouchableOpacity onPress={this.onLogin} style={{alignItems: 'center'}}>
                                <Text>Continuar</Text>
                            </TouchableOpacity>
                        </View>
                    </View>
                </SafeAreaView>
            </ImageBackground>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        // remove width and height to override fixed static size
        width: null,
        height: null,
    },
    box: {
        flex: 1,
        flexDirection: 'row',
        alignItems: 'flex-end'
    },
    boxText:{
        flexDirection: 'column',
    },
    boxContent: {
        flex:1,
        flexDirection: 'column',
        alignItems: 'stretch'
    },
    main: {
        backgroundColor: 'white',
        height: '30%',
        width: '100%',
        borderTopLeftRadius: 80,
        borderTopRightRadius: 80,
        paddingTop: 30,
        paddingHorizontal: 50,
        paddingBottom: 10,
    },
    welcome: {
        fontSize: 50,
        textAlign: 'center',
        margin: 10,
        color: 'white',
        fontWeight: '500'
    },
    description:{
        fontSize: 20,
        textAlign: 'center',
        margin: 10,
        color: 'white',
        fontWeight: '500'
    }
});
