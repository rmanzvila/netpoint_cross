
import { createStackNavigator, createAppContainer } from 'react-navigation';

import Welcome from './screens/welcome';
import Initial from './screens/home';
import New from './screens/new';
import Map from './screens/map';
import DetailMap from './screens/detailmap';
import MapMarker from './screens/mapMarker';
import Trip from './screens/trip';
import Search from './screens/search';
import App from './App';
import fixdetail from "./screens/fixdetail";

const AppNavigator = createStackNavigator({
    App: {
        screen: Welcome
    },
    Home: {
        screen: App
    },
    Initial: {
        screen: Initial
    },
    New: {
        screen: New
    },
    Map: {
        screen: MapMarker
    },
    Detail: {
        screen: DetailMap
    },
    Trip: {
        screen: Trip
    },
    FixTrip: {
        screen: fixdetail
    },
    Search: {
        screen: Search
    },
}, {
    initialRouteName: 'Initial',


});

export default createAppContainer(AppNavigator);
