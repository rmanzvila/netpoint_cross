import {ADD_TODO, NEW_TRIP, CREATE_TRIP, GET_NEW, GET_ACTUAL, ADD_POINTS, SHOW_DETAIL} from "./types";
import RealmLib from '../models/realm'
import UUIDGenerator from "react-native-uuid-generator";

export function addTodo({ title }) {

  return dispatch => {
    alert('hoaaa');
    console.log('UNA ACCCCIÓN');
  }
}

export function newTrip({ trip }) {
  console.log('el tado')

  return dispatch => {
    dispatch({
      type: NEW_TRIP,
      payload: trip
    })
  }
}

export function createTrip({ trip }, alex) {

  return (dispatch, getState) => {

    const state = getState();
    console.log(state.trip);

    RealmLib.realmHandler( realm => {
      UUIDGenerator.getRandomUUID((uuid) => {
        RealmLib.add('Trip', {
          uuid: uuid,
          animal: state.trip.animal,
          plant: state.trip.plant,
          other: state.trip.other,
          idCar: state.trip.idCar,
          inital_date: state.trip.inital_date,
          end_date: state.trip.end_date,
          latitude: trip.a.latitude,
          longitude: trip.a.longitude,
          latitude_end: trip.b.latitude,
          longitude_end: trip.b.longitude,
        })
      })
    }).then(()=>{
      alert('OK')
    });


    dispatch({
      type: CREATE_TRIP,
      payload: trip
    });
  }
}

export function getTrip(){
  return (dispatch, getState) => {
    dispatch({
      type: GET_ACTUAL,
      payload: {}
    })
  }
}

export function addPoints({points}){
  alert('SE AGREGARAN PUNTOS');
  return (dispatch, getState) => {
    dispatch({
      type: ADD_POINTS,
      payload: points
    })
  }
}

export function showDetail( uuid) {
  console.log('el detalle')

  return dispatch => {
    dispatch({
      type: SHOW_DETAIL,
      payload: {uuid}
    })
  }
}