export const ADD_TODO = 'add_todo';
export const NEW_TRIP = 'new_trip';
export const CREATE_TRIP = 'create_trip';
export const GET_NEW = 'get_new_trip';
export const GET_ACTUAL = 'get_actual';
export const ADD_POINTS = 'add_points';
export const SHOW_DETAIL = 'show_detail';
