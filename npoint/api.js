import http from './http';

async function getCurrent(latitude,longitude) {
    console.log('current?lat='+latitude+'+&lon='+longitude+'&key=9f779d6a4438439484b718b0b60e11b7&lang=es')
    return await http('current?lat='+latitude+'+&lon='+longitude+'&key=9f779d6a4438439484b718b0b60e11b7&lang=es')
}


export default {
    getCurrent,
}