import React from 'react';
import { TextInput, View, Text, StyleSheet } from 'react-native';

class Field extends React.Component {
    render() {
        return (
            <View>
                <Text style={styles.title}>{this.props.title}</Text>
                <TextInput
                    autoCapitalize="none"
                    style={[styles.field, this.props.error && styles.fieldError]}
                    onChangeText={this.props.cuandoCambie}
                    value={this.props.texto}
                />
                <Text style={[styles.text, styles.description]}>{this.props.description}</Text>
                <Text style={[styles.text, styles.error]}>{this.props.error}</Text>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    field: {
        backgroundColor: 'rgb(255,255,255)',
        borderRadius: 4,
        color: 'red',
        paddingHorizontal: 16,
        paddingVertical: 12,
        fontSize: 18,
        borderWidth: 2,
        borderColor: 'transparent',
        width: 400
    },
    description: {
        fontSize: 16
    },
    error: {
        color: 'white'
    },
    fieldError: {
        borderColor: 'red',
    },
    title: {
        fontSize: 15,
        fontFamily: 'Menlo',
        color: 'black',
        marginBottom: 10
    },
    text:{
        marginTop: 10
    }
})

export default Field
