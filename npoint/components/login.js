/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React, {Component} from 'react';
import {Platform, SafeAreaView, StyleSheet, Text, View} from 'react-native';
import Field from './field'

const instructions = Platform.select({
    ios: 'Press Cmd+R to reload,\n' + 'Cmd+D or shake for dev menu',
    android:
        'Double tap R on your keyboard to reload,\n' +
        'Shake or press menu button for dev menu',
});

type Props = {};
export default class Login extends Component{
    render() {
        return (
            <View style={styles.container}>
                <Field
                    title="Usuario"
                    description="Ingresa tu usuario"
                    cuandoCambie={(email) => this.setState({ email })}
                />
                <Field
                    title="Contraseña"
                    description="Ingresa tu contraeña ingresar"
                    cuandoCambie={(email) => this.setState({ email })}
                />
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {

    },
    welcome: {
        fontSize: 20,
        textAlign: 'center',
        margin: 10,
    },
    instructions: {
        textAlign: 'center',
        color: '#333333',
        marginBottom: 5,
    },
});
