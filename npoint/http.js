
export default async function httpGet(path) {
    const url = `http://api.weatherbit.io/v2.0/${path}`

    let response = await fetch(url, {
        method: 'GET',
    })
    return response.json()
}
