/**
 * @format
 */

import {AppRegistry} from 'react-native';
import App from './App';
import MainApp from './mainApp';
import {name as appName} from './app.json';
import Root from './Navigation';

AppRegistry.registerComponent(appName, () => MainApp );
