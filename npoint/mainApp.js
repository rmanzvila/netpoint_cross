/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React, {Component} from 'react';
import {Platform, SafeAreaView, StyleSheet, Text, View, Image, ImageBackground, TouchableOpacity} from 'react-native';
import { Provider } from 'react-redux'
import store from './store'
import Navigation from './Navigation'



export default class App extends Component<Props> {

    static navigationOptions = ({navigation}) => ({
        headerTransparent: true,
    });

    render() {
        return (

            <Provider store={store}>
                <Navigation />
            </Provider>


        );
    }
}