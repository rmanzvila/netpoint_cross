const Point = {
  name: 'Point',
  properties: {
    latitude: 'float?',
    longitude: 'float?',
    last_latitude: 'float?',
    last_longitude: 'float?',
  }
};

export default Point
