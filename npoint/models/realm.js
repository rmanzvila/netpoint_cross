import Realm from 'realm';
import Session from './session';
import Trip from './trip';
import Point from './point';


async function realmHandler (callback) {
    Realm.open({
        schema: [Session, Trip, Point],
        deleteRealmIfMigrationNeeded: true
    })
        .then(callback)
}

function add(type, data) {
    realmHandler((realm) => {
        realm.write(() => {
            realm.create(type, data);
        })
    })
}

function get(type, callback) {
    realmHandler((realm) => {
        return callback(realm.objects(type))
    })
}

function filtered(type,uuid, callback) {
    realmHandler((realm) => {
        return callback(realm.objects(type).filtered( `uuid LIKE "${uuid}"`))
    })
}

function addListener(type, callback) {
    realmHandler((realm) => {
        realm.objects(type).addListener(callback);
    })
}
function removeListeners(type){
    realmHandler((realm) =>{
        realm.removeAllListeners();
    })
}
export default {
    realmHandler,
    add,
    get,
    filtered,
    addListener,
    removeListeners,
}
