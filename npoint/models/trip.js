const Trip = {
  name: 'Trip',
  properties: {
    uuid :'string?',
    animal:'bool?',
    plant: 'bool?',
    other: 'bool?',
    idCar: 'int?',
    inital_date: 'date?',
    end_date: 'date?',
    latitude: 'float?',
    longitude: 'float?',
    latitude_end: 'float?',
    longitude_end: 'float?',
  }
};

export default Trip
