import { ADD_TODO,NEW_TRIP,CREATE_TRIP,GET_NEW, GET_ACTUAL, ADD_POINTS, SHOW_DETAIL  } from '../actions/types'

const INITIAL_STATE = {
    todos: [],
    trip: {
        animal:false,
        plant:false,
        other:false,
        idCar : null,
        initialDate: null,
        endDate: null,
        longitude: null,
        latitude: null,
    }
}

export default (state = INITIAL_STATE, action) => {
    switch (action.type) {
        case NEW_TRIP:
            return {
                ...state,
                trip: {
                    animal: action.payload.animal,
                    plant: action.payload.plant,
                    other: action.payload.other,
                    idCar: action.payload.idCar,
                    inital_date: action.payload.inital_date,
                    end_date: action.payload.end_date,
                },
                uuid: ''
            };
        case CREATE_TRIP:
            return {
                ...state,
                trip:{
                    animal: state.trip.animal,
                    plant: state.trip.plant,
                    other: state.trip.other,
                    inital_date: state.trip.inital_date,
                    end_date: state.trip.end_date,
                    latitude: action.payload.a.latitude,
                    longitude: action.payload.a.longitude,
                    latitude_end: action.payload.b.latitude,
                    longitude_end: action.payload.b.longitude
                },
                uuid: ''
            };
        case ADD_TODO:
            return {
                ...state,
                todos: [...state.todos, { title: action.payload.title }]
            };
        case GET_NEW:
            return {
                ...state,
                trip:{
                    animal: state.trip.animal,
                    plant: state.trip.plant,
                    other: state.trip.other
                }
            };
        case ADD_POINTS:
            return {
                ...state,
            };
        case GET_ACTUAL:
            return state.trip;
        case SHOW_DETAIL:
            return {
                ...state,
                uuid: action.payload.uuid
            };
        default:
            return state;
    }
}
