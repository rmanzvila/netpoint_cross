import React from 'react';
import PropTypes from 'prop-types';

import { StyleSheet, View, Text } from 'react-native';

const propTypes = {
    amount: PropTypes.number.isRequired,
    fontSize: PropTypes.number,
};

const defaultProps = {
    fontSize: 20,
};

class PriceMarker extends React.Component {
    render() {
        const { fontSize, title, end } = this.props;
        return (
            <View style={styles.container}>
                <View style={styles.bubble}>
                    <Text style={[styles.title, { fontSize }]}>{title}</Text>
                </View>
                <View style={styles.arrowBorder} />
                <View style={styles.arrow} />
            </View>
        );
    }
}

PriceMarker.propTypes = propTypes;
PriceMarker.defaultProps = defaultProps;

const styles = StyleSheet.create({
    container: {
        flexDirection: 'column',
        alignSelf: 'flex-start',
    },
    bubble: {
        flex: 0,
        flexDirection: 'row',
        alignSelf: 'flex-start',
        backgroundColor: 'lightgreen',
        padding: 2,
        borderRadius: 3,
        borderColor: 'black',
        borderWidth: 0.5,
    },
    title: {
        color: 'white',
        fontSize: 20,
    },
    arrow: {
        backgroundColor: 'transparent',
        borderWidth: 4,
        borderColor: 'transparent',
        borderTopColor: 'black',
        alignSelf: 'center',
        marginTop: -9,
    },
    arrowBorder: {
        backgroundColor: 'transparent',
        borderWidth: 4,
        borderColor: 'transparent',
        borderTopColor: 'black',
        alignSelf: 'center',
        marginTop: -0.5,
    },
});

export default PriceMarker;