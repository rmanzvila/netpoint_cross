/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React, {Component} from 'react';
import {Platform, SafeAreaView, StyleSheet, Text, View, Image, ImageBackground, TouchableOpacity} from 'react-native';
import {Card, ListItem, Button, Icon} from 'react-native-elements'
import LottieView from "lottie-react-native";
import * as Animatable from "react-native-animatable";
import {connect} from "react-redux";
import {addTodo, createTrip, getTrip} from "../actions/todos";


const instructions = Platform.select({
    ios: 'Press Cmd+R to reload,\n' + 'Cmd+D or shake for dev menu',
    android:
        'Double tap R on your keyboard to reload,\n' +
        'Shake or press menu button for dev menu',
});

Props = {};
class Detail extends Component {

    static navigationOptions = ({navigation}) => ({
        headerTransparent: true,
    });

    componentDidMount() {

        //TODO Checar si esta logeado
        this.animation.play();
        setTimeout(() => {
            this.props.navigation.navigate('Trip');
        }, 4000);


    }

    onLogin = () => {
        this.props.navigation.navigate('App')
    }

    render() {

        return (
            <ImageBackground
                source={{uri: 'https://images.unsplash.com/photo-1534030665069-90e016e995e5?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&w=1000&q=80'}}
                style={styles.container}>
                <SafeAreaView style={{flex: 1}}>
                    <View style={styles.boxText}>
                        <Animatable.Text
                            animation="bounceIn"
                            iterationCount={0}
                            direction="normal"
                            style={styles.welcome}>Detalle de trayecto
                        </Animatable.Text>
                        <Animatable.Text
                            animation="bounceIn"
                            iterationCount={0}
                            direction="normal"
                            style={styles.description}>El traslado fue creado correctamente, en breve seras redirigido a su detalle.
                        </Animatable.Text>
                    </View>
                    <View style={styles.box}>
                        <View style={styles.main}>
                            <View style={styles.boxContent}>
                                <LottieView
                                    ref={animation => {
                                        this.animation = animation;
                                    }}
                                    source={require('../1708-success.json')}
                                />
                            </View>
                        </View>


                    </View>
                </SafeAreaView>
            </ImageBackground>
        );
    }
}

function mapeoState(state){
    return {
        alexState: state
    }
}

export default connect(mapeoState,{
    addTodo,
    createTrip,
    getTrip
})(Detail);


const styles = StyleSheet.create({
    container: {
        flex: 1,
        // remove width and height to override fixed static size
        width: null,
        height: null,
    },
    box: {
        flex: 1,
        flexDirection: 'row',
        alignItems: 'flex-end'
    },
    boxText:{
        flexDirection: 'column',
    },
    boxContent: {
        flex:1,
        flexDirection: 'column',
        alignItems: 'stretch'
    },
    main: {
        backgroundColor: 'white',
        height: '30%',
        width: '100%',
        borderTopLeftRadius: 80,
        borderTopRightRadius: 80,
        paddingTop: 30,
        paddingHorizontal: 50,
        paddingBottom: 10,
    },
    welcome: {
        fontSize: 50,
        textAlign: 'center',
        margin: 10,
        marginTop: 50,
        color: 'white',
        fontWeight: '500'
    },
    description:{
        fontSize: 20,
        textAlign: 'center',
        margin: 10,
        color: 'white',
        fontWeight: '500'
    }
});
