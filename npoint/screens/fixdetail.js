/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React, {Component} from 'react';
import {Platform, SafeAreaView, StyleSheet, Text, View, Image, ImageBackground, TouchableOpacity,ScrollView} from 'react-native';
import MapView, {Marker, ProviderPropType} from 'react-native-maps';
import PointLog from "./PriceMarker";
import MapViewDirections from 'react-native-maps-directions';
import Api from '../api';
import {connect} from "react-redux";
import {addTodo, createTrip, getTrip} from "../actions/todos";
import RealmLib from "../models/realm";

const instructions = Platform.select({
    ios: 'Press Cmd+R to reload,\n' + 'Cmd+D or shake for dev menu',
    android:
        'Double tap R on your keyboard to reload,\n' +
        'Shake or press menu button for dev menu',
});

Props = {};
class Trip extends Component {

    state = {
        typeTraslade: '',
        point_a_weather: '',
        point_b_weather: '',
        point_a_weather_c: '',
        point_b_weather_c: '',
        isOpened:false,
        customTrip: {
            inital_date: '',
            end_date: ''
        }
    };

    static navigationOptions = ({navigation}) => ({
        headerTransparent: true,
        title: 'Detalle de traslado',
        headerStyle: {
            fontSize: 20,
            borderColor: 'black',
            borderWidth: .5,
            backgroundColor: '#ededed'
        },
    });

    async componentDidMount() {
        this.setState({
            isOpened:false,
        });
        if(this.props.alexState.uuid.uuid !== undefined && this.props.alexState.uuid.uuid.length > 0){
            this.setState({
                isOpened:true
            });

            await RealmLib.filtered('Trip',this.props.alexState.uuid.uuid, (aux) => {
                console.log('FILTRADO');
                let trip_local = Array.from(aux)[0];
                console.log(trip_local);
                console.log('FILTRADO');
                this.setState({
                    customTrip:{
                        animal : trip_local.animal,
                        plant : trip_local.plant,
                        other: trip_local.other,
                        latitude: trip_local.latitude,
                        longitude: trip_local.longitude,
                        longitude_end: trip_local.longitude_end,
                        latitude_end: trip_local.latitude_end,
                        inital_date: trip_local.inital_date.toString(),
                        end_date: trip_local.end_date.toString()
                    }
                });
                console.log(this.state.customTrip);

            });
        }else{
            alert(0);
            this.setState({
                customTrip:{
                    animal : this.props.alexState.trip.animal,
                    plant : this.props.alexState.trip.plant,
                    other: this.props.alexState.trip.other,
                    latitude: this.props.alexState.trip.latitude,
                    longitude: this.props.alexState.trip.longitude,
                    latitude_end: this.props.alexState.trip.latitude_end,
                    longitude_end: this.props.alexState.trip.longitude_end,
                    inital_date: this.props.alexState.trip.inital_date.toString(),
                    end_date: this.props.alexState.trip.end_date.toString(),
                }
            });


            console.log(this.props.alexState.trip.latitude_end);
            console.log('________');
        }

        if (this.state.customTrip.animal ) {
            this.setState({
                typeTraslade: 'animales'
            })
        }else{
            if (this.state.customTrip.plant ) {
                this.setState({
                    typeTraslade: 'cultivos'
                })
            }else{
                this.setState({
                    typeTraslade: 'materiales'
                })

            }
        }

        console.log(this.state);
        Api.getCurrent(this.state.customTrip.latitude,this.state.customTrip.longitude).then((weather)=>{
            console.log(weather);
            this.setState({
                point_a_weather: weather.data[0].weather.description,
                point_a_weather_c: weather.data[0].temp,
                icon_a: weather.data[0].weather.icon,
            });
        });
        Api.getCurrent(this.state.customTrip.latitude_end,this.state.customTrip.longitude_end).then((weather)=>{
            console.log(weather);
            this.setState({
                point_b_weather: weather.data[0].weather.description,
                point_b_weather_c: weather.data[0].temp,
                icon_b: weather.data[0].weather.icon,
            });
        });


    }

    onLogin = () => {
        this.props.navigation.navigate('App')
    }

    onStart = () =>{
        this.props.navigation.navigate('Search')
    }

    render() {

        return (
            <SafeAreaView style={{flex: 1, backgroundColor: '#ededed'}}>
                <ScrollView>
                    <View style={styles.boxText}>
                        <View>
                            <Image source={require(`../files/pickup.png`)} style={styles.imageCar}/>
                        </View>
                        <View>
                            <Text style={styles.description}>Traslado de {this.state.typeTraslade}</Text>
                            <Text style={styles.descriptionDate}>{this.state.customTrip.inital_date} ---  {this.state.customTrip.end_date} </Text>
                        </View>
                    </View>

                    <View style={styles.boxTextMap}>
                        <View>
                            <Text style={styles.description}>Ubicación</Text>
                            <MapView
                                style={{height: 200, width: '100%'}}
                                region={{
                                    latitude: 19.444813,
                                    longitude: -99.174721,
                                    latitudeDelta: 0.0922,
                                    longitudeDelta: 0.0421
                                }}
                                showsUserLocation={true}
                            >
                                <Marker
                                    coordinate={{latitude: this.state.customTrip.latitude,
                                        longitude: this.state.customTrip.longitude}}

                                >
                                    <PointLog title={'Inicio'}/>
                                </Marker>
                                <Marker
                                    coordinate={{latitude: this.state.customTrip.latitude_end,
                                        longitude: this.state.customTrip.longitude_end}}

                                >
                                    <PointLog title={'Fin'} end={true}/>
                                </Marker>
                                <MapViewDirections
                                    origin={{latitude: this.state.customTrip.latitude,
                                        longitude: this.state.customTrip.longitude}}
                                    destination={{latitude: this.state.customTrip.latitude_end,
                                        longitude: this.state.customTrip.longitude_end}}
                                    strokeWidth={3}
                                    strokeColor="red"
                                    apikey={'AIzaSyAbECDG2_qI3fF7Vs1bszzPkRG_xyeleJY'}
                                />
                            </MapView>

                        </View>
                        <View>

                        </View>
                    </View>
                    <View style={styles.boxTextWeather}>
                        <View>
                            <Text style={styles.description}>Condiciones climatologicas</Text>
                        </View>
                        <View style={styles.boxTextW}>
                            <View style={{backgroundColor:'white', width: '50%',height:100, alignItems: 'center'}}>
                                <Image source={{'uri':'https://www.weatherbit.io/static/img/icons/'+this.state.icon_a+'.png'}} style={styles.imageW}/>
                                <Text style={styles.descriptionW}>Inicio</Text>
                                <Text style={styles.descriptionW}>{this.state.point_a_weather}  {this.state.point_a_weather_c}º C</Text>
                            </View>
                            <View style={{backgroundColor:'white', width: '50%',height:100, alignItems: 'center'}}>
                                <Image source={{'uri':'https://www.weatherbit.io/static/img/icons/'+this.state.icon_b+'.png'}} style={styles.imageW}/>
                                <Text style={styles.descriptionW}>Destino</Text>
                                <Text style={styles.descriptionW}>{this.state.point_b_weather}  {this.state.point_b_weather_c}º C</Text>
                            </View>
                        </View>
                    </View>

                    <View style={{alignItems: 'center'}}>
                        <TouchableOpacity style={styles.submitButton} onPress={this.onStart} >
                            <Text style={{fontSize: 18, color: '#000000', textAlign: 'center'}}>Definir ruta</Text>
                        </TouchableOpacity>
                    </View>


                </ScrollView>
            </SafeAreaView>
        );
    }
}

function mapeoState(state){
    return {
        alexState: state
    }
}

export default connect(mapeoState,{
    addTodo,
    createTrip,
    getTrip
})(Trip);

const styles = StyleSheet.create({
    animateBackground:{
        height: 100,
        width: 100
    },
    container: {
        flex: 1,
        // remove width and height to override fixed static size
        width: null,
        height: null,
    },
    box: {
        flex: 1,
        flexDirection: 'row',
        alignItems: 'flex-end'
    },
    boxText:{
        backgroundColor: 'white',
        borderColor: 'black',
        borderWidth: .5,
        marginTop: 50,
        flexDirection: 'row',
    },
    boxTextW:{
        backgroundColor: 'white',
        borderColor: 'black',
        borderWidth: .5,
        flexDirection: 'row',
        justifyContent:'space-around'
    },
    boxTextButton:{
        backgroundColor: 'white',
        borderColor: 'black',
        borderWidth: .5,
        flexDirection: 'row',
        justifyContent:'space-around'
    },
    boxTextMap:{
        backgroundColor: 'white',
        borderColor: 'black',
        borderWidth: .5,
        marginTop: 40,
        flexDirection: 'column',
    },
    boxTextWeather:{
        backgroundColor: 'white',
        borderColor: 'black',
        borderWidth: .5,
        marginTop: 40,
        flexDirection: 'column',
    },
    boxContent: {
        flex:1,
        flexDirection: 'column',
        alignItems: 'stretch'
    },
    main: {
        backgroundColor: 'white',
        height: '30%',
        width: '100%',
        borderTopLeftRadius: 80,
        borderTopRightRadius: 80,
        paddingTop: 30,
        paddingHorizontal: 50,
        paddingBottom: 10,
    },
    welcome: {
        fontSize: 50,
        textAlign: 'center',
        marginTop: 50,
        color: 'white',
        fontWeight: '500'
    },
    description:{
        marginLeft: 10,
        marginTop: 10,
        fontSize: 25,
        textAlign: 'justify',
        color: 'black',
        fontWeight: '500'
    },
    descriptionDate:{
        marginLeft: 10,
        fontSize: 15,
        textAlign: 'justify',
        color: 'black',
        fontWeight: '500'
    },
    descriptionW:{
        fontSize: 18,
        textAlign: 'justify',
        color: 'black',
        fontWeight: '500'
    },
    imageCar: {
        marginLeft: 20,
        width: 100,
        height: 100,
    },
    imageW: {
        width: 60,
        height: 60,
    },
    submitButton: {
        alignItems: 'center',
        backgroundColor: 'white',
        marginTop: 40,
        padding: 10,
        borderRadius: 999,
        width: 200,
        height: 40
    }
});
