/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React, {Component} from 'react';
import {
    StyleSheet,
    Text,
    View,
    SafeAreaView,
    TouchableOpacity,
    ImageBackground,
    FlatList
} from 'react-native';
import * as Animatable from 'react-native-animatable';
import RealmLib from '../models/realm'
import {Icon} from "react-native-elements";
import {connect} from "react-redux";
import {newTrip, showDetail} from "../actions/todos";



function getIcon(animal,plant,other){
    if (animal){
        return 'paw'
    }else{
        if(plant){
            return 'leaf'
        }else{
            return 'archive'
        }
    }
}

function getIconColor(animal,plant,other){
    if (animal){
        return 'brown'
    }else{
        if(plant){
            return 'green'
        }else{
            return 'lightblue'
        }
    }
}


class Home extends Component {

    static navigationOptions = ({ navigation }) => ({
        headerTransparent: true,
        headerLeft: null
    });

    state = {
        visible: false,
        todos : [],
    };

    async componentDidMount() {
        await RealmLib.get('Trip', (aux) => {
            this.setState({
                todos : Array.from(aux)
            });
        });
    }

    onSelect = (value) => {
        this.props.showDetail({
            uuid: value
        });
        this.props.navigation.navigate('FixTrip');
    };

    onNew = () => {
        this.props.navigation.navigate('New')
    };

    render() {
        return (
            <ImageBackground
                source={require('../files/sunset.jpg')}
                style={styles.container}>
                <SafeAreaView style={{ flex: 1,  alignItems:'center'}}>
                    <View >
                        <Animatable.Text
                            animation="bounceIn"
                            iterationCount={0}
                            direction="normal"
                            style={styles.welcome}>Inicio
                        </Animatable.Text>
                        <Text style={styles.instructions}>Da clic en la opciòn deseada para continuar</Text>
                    </View>
                        <View style={styles.mainBox}>
                            <View style={{alignItems: 'center'}}>
                                <TouchableOpacity style={styles.submitButton} onPress={this.onNew} >
                                    <Text style={{fontSize: 18, color: '#000000', textAlign: 'center'}}>Nuevo traslado</Text>
                                </TouchableOpacity>
                                <TouchableOpacity style={styles.submitButton} onPress={this.onLogin} >
                                    <Text style={{fontSize: 18, color: '#000000', textAlign: 'center'}}>Historia</Text>
                                </TouchableOpacity>
                            </View>
                        </View>
                        <View style={styles.mainBoxCircle}>
                            <FlatList
                                data={this.state.todos}
                                horizontal={true}
                                renderItem={({item}) =>
                                    <TouchableOpacity style={styles.circleButton} >
                                        <Icon
                                            reverse
                                            name={getIcon(item.animal,item.plant,item.other)}
                                            type='font-awesome'
                                            color={getIconColor(item.animal,item.plant,item.other)}
                                            onPress={() => this.onSelect(item.uuid)}
                                        />
                                    </TouchableOpacity>
                                }
                                keyExtractor={item => item.email}
                            />
                        </View>
                </SafeAreaView>
            </ImageBackground>
        );
    }
}

function mapeoState(state){
    return {
        alexState: state
    }
}

export default connect(mapeoState,{
    newTrip,
    showDetail
})(Home);

const styles = StyleSheet.create({
    mainBox:{
        display: 'flex',
        flexDirection: 'column',
        justifyContent: 'center',
        height: 500
    },
    mainBoxCircle:{

    },
    container: {
        flex: 1
    },
    welcome: {
        fontSize: 30,
        textAlign: 'center',
        margin: 10,
        color:'white',
        fontWeight: '500',
        marginBottom: 40
    },
    instructions: {
        textAlign: 'center',
        color: 'white',
        fontSize: 20,
        marginBottom: 10,
        fontWeight: '500'
    },
    lottie: {
        flexDirection: 'column',
        justifyContent: 'space-between',
        alignItems:'flex-start',
        width: 400,
        height: 200,
        marginBottom:40
    },
    submitButton: {
        alignItems: 'center',
        backgroundColor: 'white',
        padding: 10,
        borderRadius: 999,
        width: 200,
        height: 40,
        marginBottom: 20
    },
    circleButton:{
        alignItems: 'center',
        backgroundColor: 'orange',
        padding: 10,
        borderRadius: 999,
        borderColor:'white',
        borderWidth: 2,
        width: 60,
        height: 60,
        margin: 10
    }
});


