/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React, {Component} from 'react';
import {
    Platform,
    StyleSheet,
    Text,
    Image,
    View,
    SafeAreaView,
    TouchableWithoutFeedback,
    TouchableOpacity,
    ImageBackground,
    FlatList
} from 'react-native';
import MapView from 'react-native-maps';



export default class App extends Component<Props> {
    static navigationOptions = ({ navigation }) => ({
        headerTransparent: true,
    });

    state = {
        latitude: 12,
        longitude: 23

    };

    componentDidMount() {
        this.watchId = navigator.geolocation.getCurrentPosition(
            (position) => {
                this.setState({
                    latitude: position.coords.latitude,
                    longitude: position.coords.longitude,
                    error: null,
                });
            },
            (error) => this.setState({ error: error.message }),
            { enableHighAccuracy: true, timeout: 20000, maximumAge: 1000 },
        );
    }

    componentWillUnmount() {
        navigator.geolocation.clearWatch(this.watchId);
    }


    render() {    return (
        <MapView
            style={{flex: 1}}
            region={{
                latitude: this.state.latitude,
                longitude: this.state.longitude,
                latitudeDelta: 0.0922,
                longitudeDelta: 0.0421
            }}
            showsUserLocation={true}
        />
        );
    }}


const styles = StyleSheet.create({
    mainBox:{
        display: 'flex',
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems:'center'
    },
    mainBoxCircle:{
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems:'center'
    },
    container: {
        flex: 1
    },
    welcome: {
        fontSize: 30,
        textAlign: 'center',
        margin: 10,
        color:'white',
        fontWeight: '500',
        marginBottom: 40
    },
    instructions: {
        textAlign: 'center',
        color: 'white',
        fontSize: 20,
        marginBottom: 10,
        fontWeight: '500'
    },
    imageCar: {
        width: 100,
        height: 100,
    },
    submitButton: {
        alignItems: 'center',
        backgroundColor: 'white',
        padding: 10,
        borderRadius: 999,
        width: 200,
        height: 40,
        marginBottom: 20
    },
    circleButton:{
        alignItems: 'center',
        backgroundColor: 'orange',
        padding: 10,
        borderRadius: 999,
        borderColor:'white',
        borderWidth: 2,
        width: 60,
        height: 60,
        margin: 10
    }
});


