/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React, {Component} from 'react';
import {
    Platform,
    StyleSheet,
    Text,
    Image,
    View,
    SafeAreaView,
    TouchableWithoutFeedback,
    TouchableOpacity,
    ImageBackground,
    FlatList,
    Dimensions
} from 'react-native';
import MapView, {Marker, ProviderPropType} from 'react-native-maps';
import PointLog from './PriceMarker';
import { connect } from 'react-redux';
import {addTodo, createTrip, getTrip, newTrip} from '../actions/todos';



const {width, height} = Dimensions.get('window');
const ASPECT_RATIO = width / height;
const LATITUDE_DELTA = 0.0922;
const LONGITUDE_DELTA = LATITUDE_DELTA * ASPECT_RATIO;
const SPACE = 0.01;

function log(eventName, e) {
    console.log(eventName, e.nativeEvent);
}

class MapMarker extends Component {
    static navigationOptions = ({navigation}) => ({
        headerTransparent: true,
    });

    state = {
        latitude: 12,
        longitude: 23

    }

    componentDidMount() {
        this.watchId = navigator.geolocation.getCurrentPosition(
            (position) => {
                this.setState({
                    latitude: position.coords.latitude,
                    longitude: position.coords.longitude,
                    error: null,
                    a: {
                        latitude: position.coords.latitude + SPACE,
                        longitude: position.coords.longitude + SPACE,
                    },
                    b: {
                        latitude: position.coords.latitude - SPACE,
                        longitude: position.coords.longitude - SPACE,

                    },
                });
            },
            (error) => this.setState({error: error.message}),
            {enableHighAccuracy: true, timeout: 20000, maximumAge: 1000},
        );
    }

    componentWillUnmount() {
        navigator.geolocation.clearWatch(this.watchId);
    }

    onSave = () => {
        alert('Se guardará el trayecto');
        this.props.createTrip({
            trip: {
                latitude:this.state.latitude,
                longitude:this.state.longitude,
                a: this.state.a,
                b: this.state.b,
                uuid: '',
            },
        },this.props.alexState);
        this.props.navigation.navigate('Detail')
    };

    updateLocation = (e) => {
        this.setState({
            b: {
                latitude: e.latitude ,
                longitude: e.longitude

            },
        });
    }

    updateLocation2 = (e) => {
        this.setState({
            a: {
                latitude: e.latitude ,
                longitude: e.longitude

            },
        });
    }



    render() {
        return (
            <View style={
                {
                    flex: 1,
                    width: '100%',
                    backgroundColor: 'transparent',

                }
            }>
                <MapView
                    style={{flex: 1}}
                    region={{
                        latitude: this.state.latitude,
                        longitude: this.state.longitude,
                        latitudeDelta: 0.0922,
                        longitudeDelta: 0.0421
                    }}
                    showsUserLocation={true}
                >
                    <Marker
                        coordinate={this.state.a}
                        onDragEnd={e => this.updateLocation2(e.nativeEvent.coordinate)}
                        draggable
                    >
                        <PointLog title={'Inicio'}/>
                    </Marker>
                    <Marker
                        coordinate={this.state.b}
                        onDragEnd={e => this.updateLocation(e.nativeEvent.coordinate)}
                        draggable
                    >
                        <PointLog title={'Fin'} end={true}/>
                    </Marker>
                </MapView>
                <View style={styles.box}>
                    <View style={styles.main}>
                        <TouchableOpacity onPress={() => this.onSave()}  style={{alignItems: 'center'}}>
                            <Text>Continuar</Text>
                        </TouchableOpacity>
                    </View>
                </View>
            </View>
        );
    }
}



function mapeoState(state){
    return {
        alexState: state
    }
}

export default connect(mapeoState,{
    addTodo,
    createTrip,
    getTrip
})(MapMarker);


const styles = StyleSheet.create({
    box: {
        backgroundColor: 'transparent',
        flexDirection: 'row',
        alignItems: 'flex-start'
    },
    buttonContainerUpDown: {
        backgroundColor: 'lightgray',
        height: 120,
        alignItems: 'center'
    },
    mainBox: {
        display: 'flex',
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center'
    },
    main: {
        backgroundColor: 'white',
        height: 90,
        width: '100%',
        paddingTop: 30,
        paddingHorizontal: 50,
        paddingBottom: 10,
    },
});


