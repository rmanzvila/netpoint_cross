/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React, {Component} from 'react';
import {
    Platform,
    StyleSheet,
    Text,
    Image,
    View,
    SafeAreaView,
    TouchableWithoutFeedback,
    TouchableOpacity,
    ImageBackground,
    FlatList
} from 'react-native';
import { Icon } from 'react-native-elements'
import * as Animatable from 'react-native-animatable';
import DatePicker from 'react-native-datepicker'
import { newTrip } from '../actions/todos';
import { connect } from 'react-redux';

const instructions = Platform.select({
    ios: 'Press Cmd+R to reload,\n' + 'Cmd+D or shake for dev menu',
    android:
        'Double tap R on your keyboard to reload,\n' +
        'Shake or press menu button for dev menu',
});

type Props = {};
class Initial extends Component {

    static navigationOptions = ({ navigation }) => ({
        headerTransparent: true,
    });

    state = {
        visible: false,
        users: [
            {
                id:1,
                image: require('../files/pickup.png'),
                cover : false
            },
            {
                id:2,
                image: require('../files/pickup-truck.png'),
                cover : false
            },
            {
                id:3,
                image: require('../files/delivery-truck.png'),
                cover : false
            },
            {
                id:3,
                image: require('../files/trailer.png'),
                cover : false
            },
        ],
        animal:false,
        plant:false,
        other:false,
        idCar : null,
        initialDate: null,
        endDate: null
    };

    componentDidMount() {
        var date = new Date().getDate();
        var month = new Date().getMonth() + 1;
        var year = new Date().getFullYear();
        this.state = {
            date : date+'-'+month+'-'+year
        }
    }

    onLogin = () => {
        if ( this.state.animal || this.state.plant || this.state.other){
            this.props.newTrip({
                trip: {
                    animal:this.state.animal,
                    plant:this.state.plant,
                    other:this.state.other,
                    idCar:this.state.idCar,
                    inital_date: this.state.initialDate,
                    end_date: this.state.endDate
                }
            },this.props.alexState);
            this.props.navigation.navigate('Map');
        } else{

            alert('Selecciona el tipo de traslado y el medio de transporte.')
        }


    };

    typeSelectec = (value) => {
      if (value == 'plant') {
          this.setState({plant:true});
          this.setState({animal:false});
          this.setState({other:false});
      }else {
          if (value == 'animal') {
              this.setState({animal:true});
              this.setState({plant:false});
              this.setState({other:false});
          }else{
              this.setState({other:true});
              this.setState({animal:false});
              this.setState({plant:false});
          }
      }
    };

    onCar = (value) => {
        this.setState({
            idCar:value
        });
    };

    render() {
        return (
            <ImageBackground
                source={{uri:'https://civileats.com/wp-content/uploads/2018/05/180515-nosb-organic-standards-change-top1.jpg'}}
                style={styles.container}>
                <SafeAreaView style={{ flex: 1,  alignItems:'center', padding:30}}>
                    <View >
                        <Animatable.Text
                            animation="bounceIn"
                            iterationCount={0}
                            direction="normal"
                            style={styles.welcome}>Nuevo Traslado
                        </Animatable.Text>
                    </View>

                    <Text style={styles.instructions}>Indica el tipo de traslado a realizar  y considera el medio de transporte necesario.</Text>

                    <View style={styles.mainBox}>
                            <TouchableOpacity >
                                <Icon
                                    reverse
                                    name='paw'
                                    type='font-awesome'
                                    color='brown'
                                    onPress={() => this.typeSelectec('animal')} />

                            </TouchableOpacity>

                            <TouchableOpacity >
                                <Icon
                                    reverse
                                    name='leaf'
                                    type='font-awesome'
                                    color='green'
                                    onPress={() => this.typeSelectec('plant')} />
                            </TouchableOpacity>

                            <TouchableOpacity >
                                <Icon
                                    reverse
                                    name='archive'
                                    type='font-awesome'
                                    color='lightblue'
                                    onPress={() => this.typeSelectec('other')} />
                            </TouchableOpacity>

                    </View>
                    <Text style={styles.instructions}>Selecciona el tipo de vehiculo.</Text>

                    <View style={styles.mainBoxCircle}>
                        <FlatList
                            data={this.state.users}
                            horizontal={true}
                            renderItem={({item}) =>
                                <TouchableOpacity style={{ margin: 10}} onPress={ () => this.onCar(item.id)} >
                                    <Image source={item.image} style={styles.imageCar}/>
                                </TouchableOpacity>
                            }
                            keyExtractor={item => item.image}
                        />
                    </View>
                    <Text style={styles.instructions}>Selecciona el periodo de partida y llegada.</Text>

                    <View>
                        <DatePicker
                            style={{width: 200}}
                            date={this.state.date}
                            mode="date"
                            placeholder="Fecha de partida"
                            format="YYYY-MM-DD"
                            minDate={this.state.date}
                            maxDate={"2020-06-01"}
                            confirmBtnText="Confirm"
                            cancelBtnText="Cancel"
                            customStyles={{
                                dateIcon: {
                                    position: 'absolute',
                                    left: 0,
                                    top: 4,
                                    marginLeft: 0
                                },
                                dateInput: {
                                    marginLeft: 36
                                }
                            }}
                            onDateChange={(date) => {this.setState({initialDate: date})}}
                        />
                        <DatePicker
                            style={{width: 200,marginTop: 10, marginBottom: 10}}
                            date={this.state.date}
                            mode="date"
                            placeholder="Fecha de llegada"
                            format="YYYY-MM-DD"
                            minDate={this.state.date}
                            maxDate={"2020-06-01"}
                            confirmBtnText="Confirm"
                            cancelBtnText="Cancel"
                            customStyles={{
                                dateIcon: {
                                    position: 'absolute',
                                    left: 0,
                                    top: 4,
                                    marginLeft: 0
                                },
                                dateInput: {
                                    marginLeft: 36
                                }
                            }}
                            onDateChange={(date) => {this.setState({endDate: date})}}
                        />
                    </View>

                    <View>
                        <TouchableOpacity style={styles.submitButton} onPress={this.onLogin} >
                            <Text style={{fontSize: 18, color: '#000000', textAlign: 'center'}}>Iniciar</Text>
                        </TouchableOpacity>
                    </View>



                </SafeAreaView>
            </ImageBackground>
        );
    }
}

function mapeoState(state){
    return {
        alexState: state
    }
}

export default connect(mapeoState,{
    newTrip
})(Initial);


const styles = StyleSheet.create({
    mainBox:{
        display: 'flex',
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems:'center'
    },
    mainBoxCircle:{
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems:'center'
    },
    container: {
        flex: 1
    },
    welcome: {
        fontSize: 30,
        textAlign: 'center',
        margin: 10,
        color:'white',
        fontWeight: '500',
        marginBottom: 40
    },
    instructions: {
        textAlign: 'center',
        color: 'white',
        fontSize: 20,
        marginBottom: 10,
        fontWeight: '500'
    },
    imageCar: {
        width: 100,
        height: 100,
    },
    submitButton: {
        alignItems: 'center',
        backgroundColor: 'white',
        padding: 10,
        borderRadius: 999,
        width: 200,
        height: 40,
        marginBottom: 20
    },
    circleButton:{
        alignItems: 'center',
        backgroundColor: 'orange',
        padding: 10,
        borderRadius: 999,
        borderColor:'white',
        borderWidth: 2,
        width: 60,
        height: 60,
        margin: 10
    }
});


