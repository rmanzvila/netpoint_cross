/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React, {Component} from 'react';
import {
    Platform,
    StyleSheet,
    Text,
    Image,
    View,
    SafeAreaView,
    TouchableWithoutFeedback,
    TouchableOpacity,
    ImageBackground,
    FlatList,
    Dimensions
} from 'react-native';
import MapView, {Marker, ProviderPropType} from 'react-native-maps';
import PointLog from './PriceMarker';
import { connect } from 'react-redux';
import {addTodo, createTrip, getTrip, newTrip, addPoints} from '../actions/todos';
import MapViewDirections from "react-native-maps-directions";
import RealmLib from "../models/realm";



const {width, height} = Dimensions.get('window');
const ASPECT_RATIO = width / height;
const LATITUDE_DELTA = 0.0922;
const LONGITUDE_DELTA = LATITUDE_DELTA * ASPECT_RATIO;
const SPACE = 0.01;
let id = 0;


function log(eventName, e) {
    console.log(eventName, e.nativeEvent);
}

class MapMarker extends Component {
    static navigationOptions = ({navigation}) => ({
        headerTransparent: true,
    });

    state = {
        latitude: 12,
        longitude: 23,
        markers: [],
        markersR: [],
        last_latitude: 0,
        last_altitude: 0

    }

    onMapPress(e) {
        this.setState({
            markers: [
                ...this.state.markers,
                {
                    coordinate: e.nativeEvent.coordinate,
                    key: id++,
                    color: 'red',
                },
            ],
            last_latitude: e.nativeEvent.coordinate.latitude,
            last_altitude: e.nativeEvent.coordinate.longitude,
        });

        if (this.state.markers.length >= 1) {
            this.setState({
                    markersR: [
                    ...this.state.markersR,
                    {
                        coordinate: e.nativeEvent.coordinate,
                        key: id++,
                        color: 'red',
                        last_latitude: this.state.last_latitude,
                        last_altitude: this.state.last_altitude,
                    },
                ],
            });
        }
        console.log(this.state)
    }

    async componentDidMount() {
        console.log('__________mappaa________');
        console.log(this.props.alexState);
        await RealmLib.filtered('Trip',this.props.alexState.uuid.uuid, (aux) => {
            console.log('FILTRADO');
            let trip_local = Array.from(aux)[0];
            console.log(trip_local);
            console.log('FILTRADO');
            this.setState({
                a: {
                    latitude: trip_local.latitude ,
                    longitude: trip_local.longitude ,
                },
                b: {
                    latitude: trip_local.latitude_end ,
                    longitude: trip_local.longitude_end ,

                }
            });
            console.log(this.state.customTrip);

        });

        this.watchId = navigator.geolocation.getCurrentPosition(
            (position) => {
                this.setState({
                    latitude: position.coords.latitude,
                    longitude: position.coords.longitude,
                    error: null,
                });
            },
            (error) => this.setState({error: error.message}),
            {enableHighAccuracy: true, timeout: 20000, maximumAge: 1000},
        );
    }

    componentWillUnmount() {
        navigator.geolocation.clearWatch(this.watchId);
    }

    onSave = () => {
        alert('Puntos a guardar');
        this.props.addPoints({});
        console.log(this.state.markers);

    }


    render() {
        return (
            <View style={
                {
                    flex: 1,
                    width: '100%',
                    backgroundColor: 'transparent',

                }
            }>
                <MapView
                    style={{flex: 1}}
                    region={{
                        latitude: this.state.latitude,
                        longitude: this.state.longitude,
                        latitudeDelta: 0.0922,
                        longitudeDelta: 0.0421
                    }}
                    showsUserLocation={true}
                    onPress={e => this.onMapPress(e)}
                >
                    {this.state.markers.map(marker => (
                        <Marker
                            key={marker.key}
                            coordinate={marker.coordinate}
                            pinColor={marker.color}
                        />
                    ))}
                    {this.state.markersR.map(markerP => (

                        <MapViewDirections

                            origin={{ latitude: markerP.last_latitude, longitude: markerP.last_altitude }}
                            destination={markerP.coordinate}
                            strokeWidth={3}
                            strokeColor="red"
                            apikey={'AIzaSyAbECDG2_qI3fF7Vs1bszzPkRG_xyeleJY'}
                        />

                    ))}


                    <Marker
                        coordinate={this.state.a}
                        onSelect={e => log('onSelect', e)}
                        onDrag={e => log('onDrag', e)}
                        onDragStart={e => log('onDragStart', e)}
                        onDragEnd={e => log('onDragEnd', e)}
                        onPress={e => log('onPress', e)}
                    >
                        <PointLog title={'Inicio'}/>
                    </Marker>
                    <Marker
                        coordinate={this.state.b}
                        onSelect={e => log('onSelect', e)}
                        onDrag={e => log('onDrag', e)}
                        onDragStart={e => log('onDragStart', e)}
                        onDragEnd={e => log('onDragEnd', e)}
                        onPress={e => log('onPress', e)}
                    >
                        <PointLog title={'Fin'} end={true}/>
                    </Marker>
                </MapView>
                <View style={styles.box}>
                    <View style={styles.main}>
                        <TouchableOpacity onPress={() => this.onSave()}  style={{alignItems: 'center'}}>
                            <Text>Continuar</Text>
                        </TouchableOpacity>
                    </View>
                </View>
            </View>
        );
    }
}



function mapeoState(state){
    return {
        alexState: state
    }
}

export default connect(mapeoState,{
    addTodo,
    createTrip,
    getTrip,
    addPoints
})(MapMarker);


const styles = StyleSheet.create({
    box: {
        backgroundColor: 'transparent',
        flexDirection: 'row',
        alignItems: 'flex-start'
    },
    buttonContainerUpDown: {
        backgroundColor: 'lightgray',
        height: 120,
        alignItems: 'center'
    },
    mainBox: {
        display: 'flex',
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center'
    },
    main: {
        backgroundColor: 'white',
        height: 90,
        width: '100%',
        paddingTop: 30,
        paddingHorizontal: 50,
        paddingBottom: 10,
    },
});


