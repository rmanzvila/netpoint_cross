/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React, {Component} from 'react';
import {
    Platform,
    StyleSheet,
    Text,
    View,
    SafeAreaView,
    TouchableWithoutFeedback,
    TouchableOpacity,
    ImageBackground
} from 'react-native';
import * as Animatable from 'react-native-animatable';
import Login from '../components/login'
import LottieView from "lottie-react-native";



const instructions = Platform.select({
    ios: 'Press Cmd+R to reload,\n' + 'Cmd+D or shake for dev menu',
    android:
        'Double tap R on your keyboard to reload,\n' +
        'Shake or press menu button for dev menu',
});

type Props = {};
export default class Welcome extends Component<Props> {

    static navigationOptions = ({ navigation }) => ({
        headerTransparent: true,
    });

    state = {
        visible: false
    };

    componentDidMount() {
        this.animation.play();
    }

    onLogin = () => {
        this.props.navigation.navigate('Initial')
    }

    render() {
        return (
            <ImageBackground
                source={{uri:'https://images.unsplash.com/photo-1534030665069-90e016e995e5?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&w=1000&q=80'}}
                style={styles.container}>
            <SafeAreaView style={{ flex: 1,  alignItems:'center'}}>
                <View style={styles.mainBox}>
                    <View>
                        <Animatable.Text
                            animation="bounceIn"
                            iterationCount={0}
                            direction="normal"
                            style={styles.welcome}>Bienvenido
                        </Animatable.Text>
                        <Text style={styles.instructions}>Ingresa tus credenciales de acceso</Text>
                    </View>
                    <View>
                        <Login/>
                    </View>
                    <View style={styles.lottie}>
                        <LottieView
                            ref={animation => {
                                this.animation = animation;
                            }}
                            source={require('../map.json')}
                        />
                    </View>
                    <View style={{alignItems: 'center'}}>
                        <TouchableOpacity style={styles.submitButton} onPress={this.onLogin} >
                            <Text style={{fontSize: 18, color: '#000000', textAlign: 'center'}}>Iniciar</Text>
                        </TouchableOpacity>
                    </View>
                </View>
            </SafeAreaView>
            </ImageBackground>
        );
    }
}

const styles = StyleSheet.create({
    mainBox:{
        display: 'flex',
        flexDirection: 'column',
        justifyContent: 'space-between',
        height: 500
    },
    container: {
        flex: 1,
    },
    welcome: {
        fontSize: 30,
        textAlign: 'center',
        margin: 10,
        color:'white',
        fontWeight: '500',
        marginBottom: 40
    },
    instructions: {
        textAlign: 'center',
        color: 'white',
        fontSize: 20,
        marginBottom: 10,
        fontWeight: '500'
    },
    lottie: {
        flexDirection: 'column',
        justifyContent: 'space-between',
        alignItems:'flex-start',
        width: 400,
        height: 200,
        marginBottom:40
    },
    submitButton: {
        alignItems: 'center',
        backgroundColor: 'white',
        padding: 10,
        borderRadius: 999,
        width: 200,
        height: 40
    }
});


