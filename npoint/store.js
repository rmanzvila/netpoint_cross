import { createStore, applyMiddleware } from 'redux'
import thunk from 'redux-thunk';
import { createLogger } from 'redux-logger';
import reducers from './reducers/reducer';

let middleware = [thunk]

if (__DEV__) {
  const logger = createLogger({ collapsed: true });
  middleware = [...middleware, logger];
} else {
  middleware = [...middleware];
}

const store = createStore(reducers, applyMiddleware(...middleware))
// store.subscribe(() => console.log(store.getState()))

export default store
